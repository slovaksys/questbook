<?php

namespace App\Presenters;

use Nette;
use Nette\DI\Container;
use App\Components\CommentFormFactory;
use App\Components\CommentFormControl;
use App\Components\CommentListFactory;
use App\Components\CommentListControl;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{

	/** @var Container @inject */
	public $container;

	/** @var CommentFormFactory @inject */
	public $commentFormFactory;

	/** @var CommentListFactory @inject */
	public $commentListFactory;

	/** @var int */
	public $replyTo;


	public function renderDefault()
	{ 

	}


	public function actionDefault(int $replyTo = null)
	{
		$this->replyTo = $replyTo;

		!$this->isAjax() ?: $this->redrawControl(); 
	}


	protected function createComponentCommentForm(): CommentFormControl
	{
		return $this->commentFormFactory->create($this->container->getService('commentFacade'));
	}


	protected function createComponentCommentList(): CommentListControl
	{
		return $this->commentListFactory->create($this->container, $this->replyTo);
	}

}
