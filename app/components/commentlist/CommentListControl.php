<?php

namespace App\Components;

use Nette;
use App\Model\CommentFacade;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;

class CommentListControl extends Nette\Application\UI\Control
{

    /** @var CommentFacade */
    private $commentFacade;

    /** @var CommentFormFactory */
    private $commentFormFactory;

    /** @var int */
    private $replyTo;


    public function __construct(CommentFacade $commentFacade,
                                CommentFormFactory $commentFormFactory,
                                int $replyTo = null)
    {
        $this->commentFacade = $commentFacade;
        $this->commentFormFactory = $commentFormFactory;
        $this->replyTo = $replyTo;
    }


    public function render(): void
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/templates/default.latte');
        $template->comments = $this->commentFacade->getComments();
        $template->replyTo = $this->replyTo;

        $template->render();
    }


    public function handleDeleteComment(int $id): void
    {
        try {
            $this->commentFacade->deleteCommentWithChildren($id);
            $this->flashmessage('Komentář byl smazán.', 'success');
        } catch (Exception $e) {
            $this->flashmessage('Při mazání došlo k chybě.', 'danger');
        }

        ($this->getPresenter()->isAjax()) ? $this->redrawControl('commentsList') : $this->redirect('this');
    }


    protected function createComponentReplyToForm(): Multiplier
    {
        return new Multiplier(function(int $replyTo) {
            return $this->commentFormFactory->create($this->commentFacade, $replyTo);
        });
    }

}
