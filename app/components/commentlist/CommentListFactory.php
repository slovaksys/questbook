<?php

namespace App\Components;

use Nette;
use Nette\DI\Container;

class CommentListFactory extends Nette\Object
{

    public function create(Container $container, 
    					   int $replyTo = null): CommentListControl
    {
        return new CommentListControl($container->getService('commentFacade'), 
        							  $container->getService('commentFormFactory'), 
        							  $replyTo);
    }

}