<?php

namespace App\Components;

use Nette;
use Nette\Utils\ArrayHash;
use App\Model\CommentFacade;
use Nette\Application\UI\Form;

class CommentFormControl extends Nette\Application\UI\Control
{

    /** @var int */
    private $replyTo;

    /** @var CommentFacade */
    private $commentFacade;


    public function __construct(CommentFacade $commentFacade, 
                                int $replyTo = null)
    {
        $this->commentFacade = $commentFacade;
        $this->replyTo = $replyTo;
    }


    public function render(): void
    {
        $template = $this->template;
        $templateFile = sprintf(__DIR__ . '/templates/%s.latte', $this->replyTo ? 'reply' : 'default'); 
        $template->setFile($templateFile);
        $template->comments = $this->commentFacade->getComments();

        $template->render();
    }


    public function createComponentCommentForm(): Form
    {
        $form = new Form();
        $form->setMethod('post');
        $form->setAction($this->getPresenter()->link('this', $replyTo = null)); // need clear replyTo for hide this form after ajax send
        
        $form->addText('username', 'Vaše jméno')
             ->setRequired('Vyplňte jméno');
        $form->addTextArea('text')
             ->setRequired('Vyplňte text zprávy');
        $form->addHidden('reply_to', $this->replyTo);

        $form->addSubmit('save', 'Odeslat');

        $form->onSuccess[] = [$this, 'commentFormSucceeded'];

        return $form;
    }


    public function commentFormSucceeded(Form $form, ArrayHash $values): void
    {
        try {
            $this->commentFacade->insertComment($values->username, $values->text, (int)$values->reply_to);
            $form->setValues([], true);

            if (!empty($this->replyTo)) {
                $this->getParent()->getParent()->flashMessage('Komentář byl přidán.', 'success'); // after send reply is this form hidden => send to CommentListControl
            } else {
                $this->flashMessage('Komentář byl přidán.', 'success');
            }

        } catch(Exception $e) {
            $this->flashMessage('Při ukládání došlo k chybě.', 'danger');
        }

        ($this->getPresenter()->isAjax()) ? $this->getPresenter()->redrawControl() : $this->redirect('this');
    }

}
