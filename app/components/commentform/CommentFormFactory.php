<?php

namespace App\Components;

use Nette;
use Nette\DI\Container;
use App\Model\CommentFacade;

class CommentFormFactory extends Nette\Object
{


    public function create(CommentFacade $commentFacade, 
    					   int $replyTo = 0): CommentFormControl
    {
        return new CommentFormControl($commentFacade, $replyTo);
    }

}
