<?php

namespace App\Model;

use Nette;
use stdClass;
use DateTime;
use Exception;
use App\Model\CommentEntity;
use Kdyby\Doctrine\EntityManager;

class CommentFacade
{

    /** @var EntityManger */
    private $entityManager;


    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function insertComment(string $username, string $text, int $parentId = 0): int
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction(); 
        
        try {
            $comment = new CommentEntity;
            $comment->setUsername($username);
            $comment->setText($text);
            $entityManager->persist($comment);
            $entityManager->flush();
            $commentId = $comment->getId();

            $selfClosure = new CommentClosureEntity; // save self closure bond
            $selfClosure->setAncestor($commentId);
            $selfClosure->setDescendant($commentId);
            $selfClosure->setDepth(0); 
            $entityManager->persist($selfClosure);
            $entityManager->flush();

            $parentClosureEntity = $entityManager->getRepository('App\Model\CommentClosureEntity')
                                           ->findBy([
                                                 'descendant' => $parentId,
                                             ]);
            foreach ($parentClosureEntity as $parentEntity) { // save parents closure bonds
                $parentClosure = new CommentClosureEntity; 
                $parentClosure->setAncestor($parentEntity->getAncestor());
                $parentClosure->setDepth($parentEntity->getDepth() + 1);
                $parentClosure->setDescendant($commentId);
                $entityManager->persist($parentClosure);
                $entityManager->flush();
            }                               

            $entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            throw $e;
        }

        return $commentId;
    }


    public function deleteCommentWithChildren(int $id): void
    {
        $entityManager = $this->entityManager;
        $entityManager->getConnection()->beginTransaction(); 
        
        try {
            $children = $this->getComments($id);

            foreach ($children as $child) {

                if (!empty($child->children)) {
                    $this->deleteCommentWithChildren($child->id); // recursive call on children
                }

                $this->deleteComment($child->id); 
            }

            $this->deleteComment($id);

            $entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $entityManager->getConnection()->rollBack();
            throw $e;
        }

    }


    public function getComments(int $parentId = 0, int $maxDeep = 1): array
    {
        $childrenQuery = $this->entityManager->createQuery('SELECT c.id, c.username, c.text, c.created, cc.depth
                                                            FROM App\Model\CommentClosureEntity cc
                                                            JOIN App\Model\CommentEntity c
                                                              WITH c.id = cc.descendant
                                                            WHERE cc.ancestor = :commentId
                                                              AND cc.depth <= 1
                                                              AND cc.ancestor != cc.descendant
                                                            ORDER BY c.id DESC')
                                             ->setParameter('commentId', $parentId);    
        $entities = $childrenQuery->getResult();
        
        $comments = [];
        foreach ($entities as $entity) {
            $comment = new stdClass;

            $comment->id = (int)$entity['id'];
            $comment->username = $entity['username'];
            $comment->text = $entity['text'];
            $comment->created = $entity['created'];
            $comment->children = $this->getComments($entity['id']);

            $comments[] = $comment;
        }

        return $comments;
    }


    private function deleteComment(int $id): void
    {
        $entityManager = $this->entityManager;


        $queryBuilder = $entityManager->getRepository('App\Model\CommentClosureEntity')->createQueryBuilder('cc'); 

        $queryBuilder->select('cc')
                     ->where('cc.ancestor = :commentId')
                     ->orWhere('cc.descendant = :commentId')
                     ->setParameter('commentId', $id);
        $childrenClosureEntities = $queryBuilder->getQuery()
                                                ->getResult();

        foreach ($childrenClosureEntities as $closureEntity) { // delete children closure bonds
            $entityManager->remove($closureEntity);    
            $entityManager->flush();    
        }
        
        $childCommentEntity = $entityManager->getRepository('App\Model\CommentEntity') // delete comment entity
                                            ->find($id);

        if (!empty($childCommentEntity)) {
            $entityManager->remove($childCommentEntity);
            $entityManager->flush();
        }
    }

}
