<?php

namespace App\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comments_closure")
 */
class CommentClosureEntity extends \Kdyby\Doctrine\Entities\BaseEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $ancestor;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $descendant;

    /**
     * @ORM\Column(type="integer")
     */
    protected $depth;


	public function setAncestor(int $id): void
	{
		$this->ancestor = $id;
	}


	public function setDescendant(int $id): void
	{
		$this->descendant = $id;
	}


	public function setDepth(int $id): void
	{
		$this->depth = $id;
	}


	public function getAncestor(): int
	{
		return $this->ancestor;
	}


	public function getDescendant(): int
	{
		return $this->descendant;
	}


	public function getDepth(): int
	{
		return $this->depth;
	}

}