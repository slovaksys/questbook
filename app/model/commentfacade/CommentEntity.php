<?php

namespace App\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comments")
 */
class CommentEntity extends \Kdyby\Doctrine\Entities\BaseEntity
{

	use \Kdyby\Doctrine\Entities\Attributes\Identifier;

    /**
     * @ORM\Column(type="string")
     */
    protected $username;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;


    public function __construct()
    {
    	$this->created = new DateTime;
    }


	public function setUsername(string $username): void
	{
		$this->username = $username;
	}


	public function getUsername(): string
	{
		return $this->username;
	}


	public function getText(): string
	{
		return $this->text;
	}


	public function getCreated(): DateTime
	{
		return $this->created;
	}

}