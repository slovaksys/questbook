<?php

namespace App;

use Nette;
use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	
	public static function createRouter(): IRouter
	{
		$router = new RouteList;
		
		$router[] = new Route('<presenter>/<action>', 'Homepage:default');
		
		return $router;
	}

}
