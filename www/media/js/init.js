$(document).ready(function() {
	$('.alert').delay(3000).fadeOut();
	
	$.nette.init();
	
	$.nette.ext('ajax', {
	  complete: function(payload) {
	      $('.alert').delay(3000).fadeOut(); // reinicialize after ajax
	  }
	});
});
