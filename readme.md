#Quest book

Require PHP >=7.1.0 

##Steps
1. clone repository git@bitbucket.org:slovaksys/questbook.git
1. run command `composer update`
1. run command `bower update`
1. create database and add user priviliegies
 1. create config/config.local.neon
 1. in `config.local.neon` create db variables (`user`, `password`, `dbname`) in namespace `doctrine`
1. run command `orm:schema-tool:create`
1. NEED! execute sql command `insert into comments_closure values (0,0,0)` for generating root comment node => TODO: doctrine fixtures